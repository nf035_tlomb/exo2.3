package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {
        List<DigitalBadgeMetadata> metadataList = new ArrayList<>();
        BufferedReader reader = media.getEncodedImageReader(false);
        String line;
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(";");
            // Assurez-vous que la ligne contient suffisamment d'éléments avant de les traiter
            if (data.length >= 3) {
                int badgeId = Integer.parseInt(data[0]);
                long walletPosition = Long.parseLong(data[1]);
                long imageSize = Long.parseLong(data[2]);
                metadataList.add(new DigitalBadgeMetadata(badgeId, walletPosition, imageSize));
            }
        }
        return metadataList;
    }

    /**
     * Lit la dernière ligne d'un fichier en accès aléatoire.
     * Cette méthode est utile pour récupérer la dernière ligne d'un fichier CSV par exemple,
     * souvent utilisée pour récupérer les dernières métadonnées ou l'amorce d'un fichier avant ajout.
     *
     * @param file Le fichier en accès aléatoire à lire.
     * @return La dernière ligne du fichier sous forme de chaîne de caractères.
     * @throws IOException Si une erreur d'entrée/sortie se produit.
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
