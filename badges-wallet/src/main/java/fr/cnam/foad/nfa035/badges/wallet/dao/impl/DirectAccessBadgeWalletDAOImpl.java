package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private final File walletDatabase;

    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    @Override
    public void addBadge(File image) throws Exception {
        ImageSerializerBase64DatabaseImpl serializer = new ImageSerializerBase64DatabaseImpl();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            serializer.serialize(image, media);
        }
    }

    @Override
    public void getBadge(OutputStream imageStream) throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws Exception {
        List<DigitalBadgeMetadata> metadataList = new ArrayList<>();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            BufferedReader reader = media.getEncodedImageReader(false);
            String line;
            while ((line = ((BufferedReader) reader).readLine()) != null) {
                String[] data = line.split(";");
                int badgeId = Integer.parseInt(data[0]);
                long walletPosition = Long.parseLong(data[1]);
                long imageSize = Long.parseLong(data[2]);
                metadataList.add(new DigitalBadgeMetadata(badgeId, walletPosition, imageSize));
            }
        }
        return metadataList;
    }

    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception {
        // Utiliser le badgeId pour identifier le badge spécifique
        // Supposons que chaque badge est stocké sur une seule ligne et que les badges sont ordonnés par badgeId
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            BufferedReader reader = media.getEncodedImageReader(false);
            String line;
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(";");
                int badgeId = Integer.parseInt(data[0]);
                if (badgeId == meta.getBadgeId()) {
                    // Nous avons trouvé le badge correspondant
                    String encodedImage = data[3]; // Supposons que l'image encodée est le 4e élément dans la ligne
                    byte[] imageBytes = Base64.getDecoder().decode(encodedImage);
                    imageStream.write(imageBytes);
                    break;
                }
            }
        }
    }

}