package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.RandomAccessFile;

public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
    long getNumberOfLines();
    void incrementLines();
}

