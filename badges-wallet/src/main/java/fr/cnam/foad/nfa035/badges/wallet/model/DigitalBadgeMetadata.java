package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * Classe représentant les métadonnées d'un badge numérique.
 */
public class DigitalBadgeMetadata {

    private int badgeId;
    private long imageSize;
    private long walletPosition;

    public DigitalBadgeMetadata() {
        // Constructeur par défaut
    }

    public DigitalBadgeMetadata(int badgeId, long walletPosition, long imageSize) {
        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;
    }

    public int getBadgeId() {
        return badgeId;
    }

    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    public long getImageSize() {
        return imageSize;
    }

    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    public long getWalletPosition() {
        return walletPosition;
    }

    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadgeMetadata)) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return badgeId == that.badgeId &&
                imageSize == that.imageSize &&
                walletPosition == that.walletPosition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(badgeId, imageSize, walletPosition);
    }

    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", imageSize=" + imageSize +
                ", walletPosition=" + walletPosition +
                '}';
    }
}

